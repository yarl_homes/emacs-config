(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes '(wheatgrass))
 '(default-frame-alist '((font . "Fira Code-12")))
 '(eshell-visual-subcommands '(("guix" "search")))
 '(geiser-mode-start-repl-p t)
 '(initial-frame-alist '((vertical-scroll-bars)))
 '(menu-bar-mode nil)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount '(1 ((shift) . 1) ((meta)) ((control) . text-scale)))
 '(safe-local-variable-values
   '((eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")
     (eval let
	   ((root-dir-unexpanded
	     (locate-dominating-file default-directory ".dir-locals.el")))
	   (when root-dir-unexpanded
	     (let*
		 ((root-dir
		   (expand-file-name root-dir-unexpanded))
		  (root-dir*
		   (directory-file-name root-dir)))
	       (unless
		   (boundp 'geiser-guile-load-path)
		 (defvar geiser-guile-load-path 'nil))
	       (make-local-variable 'geiser-guile-load-path)
	       (require 'cl-lib)
	       (cl-pushnew root-dir* geiser-guile-load-path :test #'string-equal))))
     (eval setq-local guix-directory
	   (locate-dominating-file default-directory ".dir-locals.el"))))
 '(scroll-bar-mode nil)
 '(scroll-conservatively 1)
 '(scroll-step 1)
 '(tab-always-indent 'complete)
 '(tool-bar-mode nil)
 '(visible-bell t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(server-start)

(show-paren-mode)

(global-set-key (kbd "C-<") 'text-scale-decrease)
(global-set-key (kbd "C->") 'text-scale-increase)

(global-set-key (kbd "C-S-y") 'copy-region-as-kill)

;; BETTER HELP
;; TODO: When helpful-update before helpful-buffer-switch-function,
;; change that to something like display-buffer via customization.
;; It is currently pop-to-buffer so the newly created helpful buffer gets
;; selected. I don't want that.
(load "~/.emacs.d/config/helpful.el")

;; What follows the prefix I typed?
;; (load "~/.emacs.d/config/which-key.el")

;; Completion UI / Narrowing
(load "~/.emacs.d/config/vertico.el")

;; emacs-guix
(load "~/.emacs.d/config/guix.el")

(load "~/.emacs.d/config/paredit.el")

(load "~/.emacs.d/config/yasnippet.el")

;; magit (nothing to do...)

;; marginalia : annotate completion candidates with richer information
(marginalia-mode)

;; orderless
(setq completion-styles '(orderless))

;; consult
(load "~/.emacs.d/config/consult.el")
;; consult-dir?

;; embark
(load "~/.emacs.d/config/embark.el")

;; projectile
(load "~/.emacs.d/config/projectile.el")

;; corfu
(load "~/.emacs.d/config/corfu.el")

;; ...
