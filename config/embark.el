(require 'consult)
(require 'embark)
(require 'embark-consult)
(require 'geiser)
(require 'paredit)

;; do ? after prefix and get embark help!
(setq prefix-help-command #'embark-prefix-help-command)
(setq embark-help-key (kbd "?"))

(global-set-key (kbd "C-!") 'embark-act)
(global-set-key (kbd "C-h B") 'embark-bindings)

;; those are repeat actions too now
(dolist (action '(paredit-forward paredit-backward paredit-forward-up paredit-forward-down
				  paredit-backward-up paredit-backward-down))
  (add-to-list 'embark-repeat-actions (cons action 'identifier)))

;; single key move with embark
(dolist (map (list embark-expression-map embark-identifier-map embark-defun-map))
  (dolist (key-cmd
	   '(
	     ("f" . paredit-forward)
	     ("b" . paredit-backward)
	     ("d" . paredit-forward-down)
	     ("D" . paredit-backward-down)
	     ("W" . paredit-wrap-sexp)
	     ("S" . paredit-splice-sexp)))
    (define-key map (kbd (car key-cmd)) (cdr key-cmd))))

;; When in scheme mode, delegate some work to geiser
(defun my/embark--refine-identifier-type-by-major-mode (_type target)
  (cons (or (alist-get major-mode '((scheme-mode . scheme-identifier)))
            'identifier)
        target))
(push
 '(identifier . my/embark--refine-identifier-type-by-major-mode)
 embark-transformer-alist)
(embark-define-keymap my/embark-scheme-identifier-map
  "My keymap for Embark scheme-identifier actions."
  :parent embark-identifier-map
  ("RET" geiser-edit-symbol-at-point))

(setf (alist-get 'scheme-identifier embark-keymap-alist)
      'my/embark-scheme-identifier-map)

(defun my/paredit-forward-post-hook (&rest _) (paredit-backward))

(push '(paredit-forward embark--end-of-target) embark-pre-action-hooks)
(push '(paredit-backward embark--beginning-of-target) embark-pre-action-hooks)
(push '(paredit-forward my/paredit-forward-post-hook) embark-post-action-hooks)
(push '(paredit-wrap-sexp embark--beginning-of-target) embark-pre-action-hooks)
(push '(paredit-split-sexp embark--beginning-of-target) embark-pre-action-hooks)


