;; -*- lexical-binding: t -*-

(global-unset-key (kbd "M-c"))

(global-set-key (kbd "M-c b") 'consult-buffer)
(global-set-key (kbd "C-x b") 'consult-buffer)
(global-set-key (kbd "C-x B") 'consult-project-buffer)

(global-set-key (kbd "M-c 4 b") 'consult-buffer-other-window)

(global-set-key (kbd "M-y") 'consult-yank-pop)

(global-set-key (kbd "<help> a") 'consult-apropos)

(global-set-key (kbd "M-c g") 'consult-goto-line)

(global-set-key (kbd "M-c i") 'consult-imenu)
(global-set-key (kbd "M-c I") 'consult-imenu-multi)

(global-set-key (kbd "M-c d") 'consult-find)
(global-set-key (kbd "M-c l") 'consult-line)
(global-set-key (kbd "C-s") 'consult-line)
(define-key isearch-mode-map (kbd "C-s") 'consult-line)
(global-set-key (kbd "M-c L") 'consult-line-multi)
(global-set-key (kbd "M-c g") 'consult-ripgrep)


